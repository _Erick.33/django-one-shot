from django.contrib import admin
from todos.models import TodoList, TodoItem


class TodoListAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "id",
    )


admin.site.register(TodoList, TodoListAdmin)


class TodoItemAdmin(admin.ModelAdmin):
    list_display = (
        "task",
        "due_date",
    )


admin.site.register(TodoItem, TodoItemAdmin)
